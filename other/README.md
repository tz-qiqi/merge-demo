# 每天课前小故事

> 第一节课（开班典礼）无课前故事

## 第二节课

**主题：参加了一次相亲，感觉像参加了一场面试**

当时那个我都觉得是不是参加了一场面试，那个人问了我年龄，问了我学历，从头到尾问我工作年限以及有无工作经验。就相当于你去面试，面试官可能更着重的问你的一些问题。但是你相亲他可能问的就是你的学历、工作、时间，就像查户口，基本上把你家里的情况都查的差不多了啊。我前几天相亲，当时他问了我一个让我不太高兴的问题：你是那个学校毕业的。当时我在想，我都毕业这么多年了，居然还有人问我是从哪个学校毕业的。然后我当时就告诉他，我是在某理工大学毕业的。当时他就说，这个学校还可以啊，不错啊，然后就开始问工作，问一切。

感觉吃这顿饭还是挺开心的，聊天也相当的愉快。后来我问了一下我爸，我爸就跟我说：“你这次相亲你感觉男孩子怎么样呀？”

“还行吧，一般般吧，男孩子挺踏实的也挺稳重的。”

“你是觉得人家还不错了，人家是对你不怎么满意”

我当时就觉得不对呀，我的言行举止我都觉得很OK呀，挺稳重的呀。最后他给我来了一句“人家嫌弃你胖了”，然后当时我瞬间就觉得自己遭受了一种重大的打击，因为我真的是挺胖的啊，我不知道有没有人看到过我没有美颜的照片，我真的是胖了很多啊，让我的自尊心受到了打击，就很难受。我当时就在想，我一天天坐在这也不运动，所以我就下定决心，今年第一个目标就是减肥，瘦到100斤吧。

伤害性很大，侮辱性也很强，他对我的言谈举止、做人风格都没有什么多大意见，问题就是你没有一个好的形象。我当时就在想，你家崽啥学历呀，你家崽也没很大的技术呀，凭啥呀。我当时心里是这么想的嘛，你还好意思吐槽我，家里不久两个臭钱嘛，谁家没钱似的。但是后来想一想，的确，现在对女性，很多人都说男女平等，但是对女性很多的歧视是存在的啊，包括一些对男性的歧视也是存在的。你没有能力，光看家里，那我也是看不上你的。

所以说，希望大家能在新的一年里，不断的提升自己，不要让自己的一个不是很完美的点成为人家抨击你的一个话题吧。

## 第三节课

（待整理）